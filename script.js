"use strict"

//  КЛАСС

class Hamburger {
    constructor(size, stuffing) {
        Object.defineProperty(this, "size", {
            value: size,
            configurable: true,
            writable: false,
            enumerable: true
        });

        Object.defineProperty(this, "stuffing", {
            value: stuffing,
            configurable: true,
            writable: false,
            enumerable: true
        });

        this.allTopping = []
    };

    get Toppings() {
        console.log(this.allTopping);
    }

    set Toppings(topping) {
        try {
            for (let key = 0; key < this.allTopping.length; key++) {
                if (this.allTopping.includes(topping)) {
                    throw new HamburgerException("Add Error.topping already exists");
                }
            }
        } catch (e) {}
        if (this.allTopping.length == 0) {
            this.allTopping.push(topping);
        } else {
            for (let key = 0; key < this.allTopping.length; key++) {
                if (!this.allTopping.includes(topping)) {
                    this.allTopping.push(topping);
                }
            }
        }
    }


    removeTopping(topping) {
        try {
            if (!this.allTopping.includes(topping)) {
                throw new HamburgerException("Delete Error.no topping");
            }
        } catch (e) {}

        for (let key = 0; key < this.allTopping.length; key++) {
            if (this.allTopping[key] == topping) {
                this.allTopping.splice(key, 1)
            };
        }
    }

    getSize() {
        console.log(this.size);
    }

    /* Узнать начинку гамбургера */

    getStuffing() {
        console.log(this.stuffing);
    }


    // Узнать цену гамбургера  Цена в тугриках

    calculatePrice() {
        let priceAll = [];
        for (let key in this.stuffing) {
            priceAll.push(this.stuffing[key]['price'])
        }
        for (let key in this.size) {
            priceAll.push(this.size[key]['price'])
        }

        for (let key = 0; key < this.allTopping.length; key++) {
            var priceTop = this.allTopping[key]
            for (let mey in this.allTopping[key]) {
                priceAll.push(this.allTopping[key][mey]['price'])
            }
        }
        return priceAll.reduce(function (sum, current) {
            return sum + current;
        })
    };


    calculateCalories() {
        let calAll = [];
        for (let key in this.stuffing) {
            calAll.push(this.stuffing[key]['cal'])
        }
        for (let key in this.size) {
            calAll.push(this.size[key]['cal'])
        }

        for (let key = 0; key < this.allTopping.length; key++) {
            var priceTop = this.allTopping[key]
            for (let mey in this.allTopping[key]) {
                calAll.push(this.allTopping[key][mey]['cal'])
            }
        };
        return calAll.reduce(function (sum, current) {
            return sum + current;
        });
    };



}
/* Размеры, виды начинок и добавок  */


Hamburger.SIZE_SMALL = {
    small: {
        "price": 50,
        "cal": 20
    }
};
Hamburger.SIZE_LARGE = {
    "big": {
        "price": 100,
        "cal": 40
    }
}
Hamburger.STUFFING_CHEESE = {
    "Cheese": {
        "price": 10,
        "cal": 20
    }
}
Hamburger.STUFFING_SALAD = {
    "Salad": {
        "price": 20,
        "cal": 5
    }
}
Hamburger.STUFFING_POTATO = {
    "Potato": {
        "price": 15,
        "cal": 10
    }
}
Hamburger.TOPPING_MAYO = {
    "mayo": {
        "price": 20,
        "cal": 5
    }
}
Hamburger.TOPPING_SPICE = {
    "spice": {
        "price": 15,
        "cal": 0
    }
}


// переменные управления значений в интерфейсе

let small = Hamburger.SIZE_SMALL
let big = Hamburger.SIZE_LARGE
let Cheese = Hamburger.STUFFING_CHEESE
let Salad = Hamburger.STUFFING_SALAD
let Potato = Hamburger.STUFFING_POTATO
let mayo = Hamburger.TOPPING_MAYO
let spice = Hamburger.TOPPING_SPICE


// Блок управления интерфейсом

start_order.onclick = function () {
    start_order.style.display = "none";
    burg_size.style.display = "flex";
};

let sizeBurg = document.getElementsByClassName("enter_size")[0];


// Создаем новый заказ

let newHamburger;
sizeBurg.onclick = function () {

    try { //проверка выбраного размера
        let w = form_size.getElementsByTagName("input")
        for (var i = 0; i < w.length; i++) {
            if (w[i].checked) {
                valSize.value = w[i].value
                valSize.style.backgroundColor = "#FFFF00"
            };
        }
        // проверка выбраной добавки
        let t = form_stuffing.getElementsByTagName("input")
        for (var j = 0; j < t.length; j++) {
            if (t[j].checked) {
                valStuffing.value = t[j].value
                valStuffing.style.backgroundColor = "#FFFF00"
            };
        }
        if (valStuffing.value == false || valSize.value == false) {
            throw new HamburgerException("You have not chosen the size and / or stuffing of the hamburger. Please make your choice.");
        }
    } catch (e) {
        return
    }

    // Запуск Конструктор Гамбургера

    newHamburger = new Hamburger(eval(valSize.value), eval(valStuffing.value));

    burg_size.style.display = "none";
    burg_toping.style.display = "flex";
    console.log(newHamburger)
}


let adMay = document.getElementsByClassName("add_mayo")[0];
let delMay = document.getElementsByClassName("delete_mayo")[0];
let adSpi = document.getElementsByClassName("add_spice")[0];
let delSpi = document.getElementsByClassName("delete_spice")[0];









// Добавить добавку к гамбургеру. Можно добавить несколько
//
adMay.onclick = function () {
    newHamburger.Toppings = mayo
    valMayo.value = "Yes";
    valMayo.style.backgroundColor = "#00FF7F";
};

delMay.onclick = function () {
    valMayo.value = "No";
    newHamburger.removeTopping(mayo);
    valMayo.style.backgroundColor = "#FFA07A";
};
adSpi.onclick = function () {
    valSpice.value = "Yes";
    newHamburger.Toppings = spice;
    valSpice.style.backgroundColor = "#00FF7F";
};
delSpi.onclick = function () {
    valSpice.value = "No";
    newHamburger.removeTopping(spice);
    valSpice.style.backgroundColor = "#FFA07A";
};



// Убрать добавку, при условии, что она ранее была


save_order.onclick = function () {
    burg_toping.style.display = "none";
    finaly.style.display = "flex";

    /* Получить список добавок. */
    newHamburger.Toppings;
    newHamburger.getSize()
    newHamburger.getStuffing()
}


calories.onclick = function () {
    valCalories.value = newHamburger.calculateCalories() + " " + "Ккал."
}
prise.onclick = function () {
    valPrice.value = newHamburger.calculatePrice() + " " + "грн."
}


// Представляет информацию об ошибке в ходе работы с гамбургером.


function HamburgerException(message) {
    report.value = message;
    report.style.backgroundColor = "#FFB6C1"
    setTimeout(function () {
        report.value = "";
        report.style.backgroundColor = "#ffffff"
    }, 1500);
}
